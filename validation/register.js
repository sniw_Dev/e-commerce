/** @format */

const Validator = require('validator');
const isEmpty = require('is-empty');
module.exports = function validateRegisterInput(data) {
  let errors = {};
  // Convert empty fields to an empty string so we can use validator functions
  data.fullname = !isEmpty(data.fullname) ? data.fullname : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.confirmpassword = !isEmpty(data.confirmpassword)
    ? data.confirmpassword
    : '';
  data.phone = !isEmpty(data.phone) ? data.phone : '';

  // Name checks
  if (Validator.isEmpty(data.fullname)) {
    errors.fullname = 'Name field is required';
  }
  if (!Validator.isLength(data.fullname, { min: 2, max: 30 })) {
    errors.fullname = 'Name must be between 2 and 30 characters';
  }
  // Email checks
  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email field is required';
  } else if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid';
  }
  // Password checks
  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  }
  if (Validator.isEmpty(data.confirmpassword)) {
    errors.confirmpassword = 'Confirm password field is required';
  }
  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Password must be at least 6 characters';
  }
  if (!Validator.equals(data.password, data.confirmpassword)) {
    errors.confirmpassword = 'Passwords must match';
  }

  if (!Validator.isLength(data.phone, { min: 8, max: 8 })) {
    errors.phone = 'Phone number must be 8 digits';
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
