/** @format */

import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  UPDATE_CART,
  RESET_CART,
  UPDATE_CART_ITEM,
  UPDATE_CART_ITEM_PRICE,
  UPDATE_CART_ITEM_QUANTITY,
  TOGGLE_CART,
  CLOSE_CART,
} from '../types/cart/Cart';

export const ADD_ITEM_TO_CART = (cart) => (dispatch) => {
  console.log('called', cart);
  dispatch({
    type: ADD_TO_CART,
    payload: cart,
  });
};

export const REMOVE_ITEM_FROM_CART = (cart) => (dispatch) => {
  dispatch({
    type: REMOVE_FROM_CART,
    payload: cart,
  });
};

export const UPDATE_ITEM_CART = (cart) => (dispatch) => {
  dispatch({
    type: UPDATE_CART,
    payload: cart,
  });
};

export const RESET_ITEM_CART = () => (dispatch) => {
  dispatch({
    type: RESET_CART,
  });
};

export const UPDATE_ITEM_CART_PRODUCT = (cart) => (dispatch) => {
  dispatch({
    type: UPDATE_CART_ITEM,
    payload: cart,
  });
};

export const UPDATE_CART_ITEM_PRICE_TOTAL = (total) => (dispatch) => {
  dispatch({
    type: UPDATE_CART_ITEM_PRICE,
    payload: total,
  });
};

export const TOGGLE_CART_MENU = () => (dispatch) => {
  dispatch({
    type: TOGGLE_CART,
  });
};

export const CLOSE_CART_MENU = () => (dispatch) => {
  dispatch({
    type: CLOSE_CART,
  });
};
