/** @format */

import { combineReducers } from 'redux';
import CartReducer from './CartReducer';
import MessagesReducer from './MessageReducer';
import AuthReducer from './AuthReducer';
import ErrorReducer from './ErrorReducer';
import SuccessReducer from './SucessReducer';
import HeroReducer from './HeroReducer';
export default combineReducers({
  cart: CartReducer,
  messages: MessagesReducer,
  auth: AuthReducer,
  errors: ErrorReducer,
  success: SuccessReducer,
  hero: HeroReducer,
});
