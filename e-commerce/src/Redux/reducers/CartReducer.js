/** @format */

import {
  RESET_CART,
  ADD_TO_CART,
  REMOVE_FROM_CART,
  UPDATE_CART,
  UPDATE_CART_ITEM,
  UPDATE_CART_ITEM_QUANTITY,
  UPDATE_CART_ITEM_PRICE,
  TOGGLE_CART,
  CLOSE_CART,
} from '../types/cart/Cart';

const initialState = {
  cart: [],
  total: 0,
  open: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case RESET_CART:
      return {
        ...state,
        cart: [],
      };
    case ADD_TO_CART:
      return {
        ...state,
        cart: action.payload,
      };
    case REMOVE_FROM_CART:
      return {
        ...state,
        cart: action.payload,
      };
    case UPDATE_CART:
      return {
        ...state,
        cart: action.payload,
      };
    case UPDATE_CART_ITEM:
      return {
        ...state,
        cart: action.payload,
      };
    case UPDATE_CART_ITEM_QUANTITY:
      return {
        ...state,
        cart: action.payload,
      };
    case UPDATE_CART_ITEM_PRICE:
      return {
        ...state,
        total: action.payload,
      };

    case TOGGLE_CART: {
      return {
        ...state,
        open: !state.open,
      };
    }
    case CLOSE_CART: {
      return {
        ...state,
        open: false,
      };
    }
    default:
      return state;
  }
}
