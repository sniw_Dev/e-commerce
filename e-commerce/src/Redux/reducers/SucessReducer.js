/** @format */

import { REGISTER_SUCCESS } from '../types/user/user';
const initialState = {};
export default function (state = initialState, action) {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}
