/** @format */

import { UPDATE_HERO } from '../types/Hero/Hero';
const initialState = {
  img: '',
};
export default function (state = initialState, action) {
  switch (action.type) {
    case UPDATE_HERO:
      return {
        ...state,
        img: action.payload,
      };
    default:
      return state;
  }
}
