/** @format */

import {
  DISPLAY_ERRORS,
  DISPLAY_SUCCESS,
  DISPLAY_WARNING,
  DISPLAY_INFO,
  DISPLAY_MESSAGE,
} from '../types/messages/Messages';

const initialState = {
  Message: '',
  Type: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case DISPLAY_ERRORS:
      return {
        ...state,
        Message: action.payload,
        Type: 'ERRORS',
      };
    case DISPLAY_SUCCESS:
      return {
        ...state,
        Message: action.payload,
        Type: 'SUCCESS',
      };
    case DISPLAY_WARNING:
      return {
        ...state,
        Message: action.payload,
        Type: 'WARNING',
      };
    case DISPLAY_INFO:
      return {
        ...state,
        Message: action.payload,
        Type: 'INFO',
      };
    case DISPLAY_MESSAGE:
      return {
        ...state,
        Message: action.payload,
        Type: 'MESSAGE',
      };

    default:
      return {
        ...state,
        Message: '',
        Type: '',
      };
  }
}
