/** @format */

import thunk from 'redux-thunk';
import state from './reducers';
import logger from 'redux-logger';
import { configureStore } from '@reduxjs/toolkit';

const initialState = {};

export default configureStore({
  reducer: state,
  middleware: [thunk, logger],
  devTools: true,
  initialState,
});
