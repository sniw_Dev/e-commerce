/** @format */

import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Pages/login/Login';
import Navbar from './Components/navbar/Navbar';
import Footer from './Components/footer/Footer';
import Product from './Pages/products/Product';
import ProductDetail from './Pages/product_detail/ProductDetail';
import Checkout from './Pages/chekout/Checkout';
import Home from './Pages/home/Home';
import Cartcheckout from './Pages/bigcheckout/Cartcheckout';
import Signup from './Pages/signup/Signup';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './Redux/actions/Actionuser';
import { connect } from 'react-redux';
import Store from './Redux/Store';
import PropTypes from 'prop-types';

// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  Store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    Store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = './login';
  }
}

const ProtectedRoute = ({ user, children }) => {
  if (!user) {
    return <Navigate to='/login' replace />;
  }

  return children;
};

function App(props) {
  return (
    <div className='App'>
      <BrowserRouter>
        <Navbar />
        <Checkout />
        <Routes>
          <Route path='/' element={<Home />}>
            {/* <Route index element={<Home />} />
            <Route path='teams' element={<Teams />}>
              <Route path=':teamId' element={<Team />} />
              <Route path='new' element={<NewTeamForm />} />
              <Route index element={<LeagueStandings />} />
            </Route> */}
          </Route>
          <Route path='products' element={<Product />}></Route>
          <Route path='products/:productId' element={<ProductDetail />} />
          <Route path='login' element={<Login />} />
          <Route path='signup' element={<Signup />} />
          <Route
            path='cart'
            element={
              <ProtectedRoute user={props.auth.isAuthenticated}>
                <Cartcheckout />
              </ProtectedRoute>
            }
          />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

App.propTypes = {
  auth: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, {})(App);
