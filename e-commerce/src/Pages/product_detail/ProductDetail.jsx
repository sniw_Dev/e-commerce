/** @format */

import React, { useEffect, useState } from 'react';
import './Productdetail.css';
import { useLocation } from 'react-router-dom';
import {
  ADD_ITEM_TO_CART,
  UPDATE_CART_ITEM_PRICE_TOTAL,
} from '../../Redux/Actions/Actioncart';
import { useFormik } from 'formik';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const ProductDetail = (props) => {
  const location = useLocation();
  const [product, setProduct] = useState({});
  const formik = useFormik({
    initialValues: {
      Quantity: 1,
    },
  });

  useEffect(() => {
    setProduct(location.state.product);
  }, [location.state.product]);

  useEffect(() => {
    console.log(props.shopcart);
  }, [props.shopcart]);
  const add_to_cart_handler = (e) => {
    let itemcart = {
      product: product,
      quantity: formik.values.Quantity,
    };
    let arraycart = props.shopcart.cart;
    console.log(arraycart);
    arraycart.push(itemcart);
    props.shopcart.cart = arraycart;
    update_cart_total(arraycart);
    props.ADD_ITEM_TO_CART(arraycart);
  };

  const update_cart_total = (prodlist) => {
    console.log(prodlist);
    let sum = 0;
    prodlist.map((item) => {
      let price = parseFloat(item.product.price.substr(1));
      console.log(price);
      sum += price * item.quantity;
    });
    props.UPDATE_CART_ITEM_PRICE_TOTAL(sum);
    console.log('total', props.shopcart.total, sum);
  };
  return (
    <div>
      <section>
        <div class='relative max-w-screen-xl px-4 py-8 mx-auto'>
          <div class='grid items-start grid-cols-1 gap-8 md:grid-cols-2'>
            <div class='grid grid-cols-2 gap-4 md:grid-cols-1'>
              <div class='aspect-w-1 aspect-h-1'>
                <img
                  alt='Mobile Phone Stand'
                  class='object-cover rounded-xl'
                  src={'https://nextui.org' + product.img}
                />
              </div>

              <div class='grid grid-cols-2 gap-4 lg:mt-4'>
                <div class='aspect-w-1 aspect-h-1'>
                  <img
                    alt='Mobile Phone Stand'
                    class='object-cover rounded-xl'
                    src={'https://nextui.org' + product.img}
                  />
                </div>

                <div class='aspect-w-1 aspect-h-1'>
                  <img
                    alt='Mobile Phone Stand'
                    class='object-cover rounded-xl'
                    src={'https://nextui.org' + product.img}
                  />
                </div>

                <div class='aspect-w-1 aspect-h-1'>
                  <img
                    alt='Mobile Phone Stand'
                    class='object-cover rounded-xl'
                    src={'https://nextui.org' + product.img}
                  />
                </div>

                <div class='aspect-w-1 aspect-h-1'>
                  <img
                    alt='Mobile Phone Stand'
                    class='object-cover rounded-xl'
                    src={'https://nextui.org' + product.img}
                  />
                </div>
              </div>
            </div>

            <div class='sticky top-0'>
              <div class='flex justify-between mt-8'>
                <div class='max-w-[35ch]'>
                  <h1 class='text-2xl font-bold'>{product.title}</h1>

                  <div class='flex mt-2 -ml-0.5'>
                    <svg
                      class='w-5 h-5 text-yellow-400'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                      fill='currentColor'>
                      <path d='M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z' />
                    </svg>

                    <svg
                      class='w-5 h-5 text-yellow-400'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                      fill='currentColor'>
                      <path d='M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z' />
                    </svg>

                    <svg
                      class='w-5 h-5 text-yellow-400'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                      fill='currentColor'>
                      <path d='M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z' />
                    </svg>

                    <svg
                      class='w-5 h-5 text-yellow-400'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                      fill='currentColor'>
                      <path d='M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z' />
                    </svg>

                    <svg
                      class='w-5 h-5 text-gray-200'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                      fill='currentColor'>
                      <path d='M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z' />
                    </svg>
                  </div>
                </div>

                <p class='text-lg font-bold'>{product.price}</p>
              </div>

              <details class='relative mt-4 group'>
                <summary class='block'>
                  <div>
                    <div class='prose max-w-none group-open:hidden'>
                      <p>{product.description}</p>
                    </div>

                    <span class='mt-4 text-sm font-medium underline cursor-pointer group-open:absolute group-open:bottom-0 group-open:left-0 group-open:mt-0'>
                      Read More
                    </span>
                  </div>
                </summary>

                <div class='pb-6 prose max-w-none'>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Ipsa veniam dicta beatae eos ex error culpa delectus rem
                    tenetur, architecto quam nesciunt, dolor veritatis nisi
                    minus inventore, rerum at recusandae?
                  </p>

                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Placeat nam sapiente nobis ea veritatis error consequatur
                    nisi exercitationem iure laudantium culpa, animi temporibus
                    non! Maxime et quisquam amet. A, deserunt!
                  </p>
                </div>
              </details>

              <div class='mt-12'>
                <div class='flex mt-12'>
                  <div>
                    <label for='quantity' class='sr-only'>
                      Qty
                    </label>

                    <input
                      type='number'
                      id='quantity'
                      min='1'
                      name='Quantity'
                      class='w-12 py-3 text-xs text-center border-gray-200 rounded no-spinners'
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.Quantity}
                    />
                  </div>

                  <button
                    onClick={(e) => {
                      add_to_cart_handler();
                    }}
                    class='block px-5 py-3 ml-3 text-xs font-medium text-white bg-green-600 rounded hover:bg-green-500'>
                    Add to Cart
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};
ProductDetail.propTypes = {
  ADD_ITEM_TO_CART: PropTypes.func.isRequired,
  shopcart: PropTypes.object.isRequired,
  UPDATE_CART_ITEM_PRICE_TOTAL: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  shopcart: state.cart,
  msg: state.messages,
});

export default connect(mapStateToProps, {
  ADD_ITEM_TO_CART,
  UPDATE_CART_ITEM_PRICE_TOTAL,
})(ProductDetail);
