/** @format */

import React from 'react';
import { Card, Grid, Row, Text } from '@nextui-org/react';
import { useNavigate } from 'react-router-dom';

const Product = () => {
  let navigate = useNavigate();

  window.addEventListener('resize', () => {
    const desktopScreen = window.innerWidth < 768;

    document.querySelector('details').open = !desktopScreen;
  });
  const list = [
    {
      title: 'Orange',
      img: '/images/fruit-1.jpeg',
      price: '$5.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Tangerine',
      img: '/images/fruit-2.jpeg',
      price: '$3.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Raspberry',
      img: '/images/fruit-3.jpeg',
      price: '$10.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon',
      img: '/images/fruit-4.jpeg',
      price: '$5.30',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Advocato',
      img: '/images/fruit-5.jpeg',
      price: '$15.70',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon 2',
      img: '/images/fruit-6.jpeg',
      price: '$8.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Banana',
      img: '/images/fruit-7.jpeg',
      price: '$7.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Watermelon',
      img: '/images/fruit-8.jpeg',
      price: '$12.20',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Orange',
      img: '/images/fruit-1.jpeg',
      price: '$5.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Tangerine',
      img: '/images/fruit-2.jpeg',
      price: '$3.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Raspberry',
      img: '/images/fruit-3.jpeg',
      price: '$10.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon',
      img: '/images/fruit-4.jpeg',
      price: '$5.30',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Advocato',
      img: '/images/fruit-5.jpeg',
      price: '$15.70',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon 2',
      img: '/images/fruit-6.jpeg',
      price: '$8.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Banana',
      img: '/images/fruit-7.jpeg',
      price: '$7.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Watermelon',
      img: '/images/fruit-8.jpeg',
      price: '$12.20',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Orange',
      img: '/images/fruit-1.jpeg',
      price: '$5.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Tangerine',
      img: '/images/fruit-2.jpeg',
      price: '$3.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Raspberry',
      img: '/images/fruit-3.jpeg',
      price: '$10.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon',
      img: '/images/fruit-4.jpeg',
      price: '$5.30',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Advocato',
      img: '/images/fruit-5.jpeg',
      price: '$15.70',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon 2',
      img: '/images/fruit-6.jpeg',
      price: '$8.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Banana',
      img: '/images/fruit-7.jpeg',
      price: '$7.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Watermelon',
      img: '/images/fruit-8.jpeg',
      price: '$12.20',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
  ];

  const navigate_to_product_detail = (product) => {
    navigate(`${product.title}`, { state: { product: product } });
  };

  return (
    <div>
      <section>
        <div class='max-w-screen-xl px-4 py-12 mx-auto sm:px-6 lg:px-8'>
          <div class='grid grid-cols-1 gap-4 lg:grid-cols-4 lg:items-start'>
            <div class='lg:sticky lg:top-4'>
              <details
                open
                class='overflow-hidden border border-gray-200 rounded'>
                <summary class='flex items-center justify-between px-5 py-3 bg-gray-100 lg:hidden'>
                  <span class='text-sm font-medium'>Toggle Filters</span>

                  <svg
                    class='w-5 h-5'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'>
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M4 6h16M4 12h16M4 18h16'
                    />
                  </svg>
                </summary>

                <form action='' class='border-t border-gray-200 lg:border-t-0'>
                  <fieldset>
                    <legend class='block w-full px-5 py-3 text-xs font-medium bg-gray-50'>
                      Type
                    </legend>

                    <div class='px-5 py-6 space-y-2'>
                      <div class='flex items-center'>
                        <input
                          id='toy'
                          type='checkbox'
                          name='type[toy]'
                          class='w-5 h-5 border-gray-300 rounded'
                        />

                        <label for='toy' class='ml-3 text-sm font-medium'>
                          Toy
                        </label>
                      </div>

                      <div class='flex items-center'>
                        <input
                          id='game'
                          type='checkbox'
                          name='type[game]'
                          class='w-5 h-5 border-gray-300 rounded'
                        />

                        <label for='game' class='ml-3 text-sm font-medium'>
                          Game
                        </label>
                      </div>

                      <div class='flex items-center'>
                        <input
                          id='outdoor'
                          type='checkbox'
                          name='type[outdoor]'
                          class='w-5 h-5 border-gray-300 rounded'
                        />

                        <label for='outdoor' class='ml-3 text-sm font-medium'>
                          Outdoor
                        </label>
                      </div>

                      <div class='pt-2'>
                        <button
                          type='button'
                          class='text-xs text-gray-500 underline'>
                          Reset Type
                        </button>
                      </div>
                    </div>
                  </fieldset>

                  <div>
                    <fieldset>
                      <legend class='block w-full px-5 py-3 text-xs font-medium bg-gray-50'>
                        Age
                      </legend>

                      <div class='px-5 py-6 space-y-2'>
                        <div class='flex items-center'>
                          <input
                            id='3+'
                            type='checkbox'
                            name='age[3+]'
                            class='w-5 h-5 border-gray-300 rounded'
                          />

                          <label for='3+' class='ml-3 text-sm font-medium'>
                            3+
                          </label>
                        </div>

                        <div class='flex items-center'>
                          <input
                            id='8+'
                            type='checkbox'
                            name='age[8+]'
                            class='w-5 h-5 border-gray-300 rounded'
                          />

                          <label for='8+' class='ml-3 text-sm font-medium'>
                            8+
                          </label>
                        </div>

                        <div class='flex items-center'>
                          <input
                            id='12+'
                            type='checkbox'
                            name='age[12+]'
                            class='w-5 h-5 border-gray-300 rounded'
                          />

                          <label for='12+' class='ml-3 text-sm font-medium'>
                            12+
                          </label>
                        </div>

                        <div class='flex items-center'>
                          <input
                            id='16+'
                            type='checkbox'
                            name='age[16+]'
                            class='w-5 h-5 border-gray-300 rounded'
                          />

                          <label for='16+' class='ml-3 text-sm font-medium'>
                            16+
                          </label>
                        </div>

                        <div class='pt-2'>
                          <button
                            type='button'
                            class='text-xs text-gray-500 underline'>
                            Reset Age
                          </button>
                        </div>
                      </div>
                    </fieldset>
                  </div>

                  <div class='flex justify-between px-5 py-3 border-t border-gray-200'>
                    <button
                      name='reset'
                      type='button'
                      class='text-xs font-medium text-gray-600 underline rounded'>
                      Reset All
                    </button>

                    <button
                      name='commit'
                      type='button'
                      class='px-5 py-3 text-xs font-medium text-white bg-green-600 rounded'>
                      Apply Filters
                    </button>
                  </div>
                </form>
              </details>
            </div>

            <div class='lg:col-span-3'>
              <div class='flex items-center justify-between'>
                <p class='text-sm text-gray-500'>
                  <span class='hidden sm:inline'>Showing</span>6 of 24 Products
                </p>

                <div class='ml-4'>
                  <label for='SortBy' class='sr-only'>
                    Sort
                  </label>

                  <select
                    id='SortBy'
                    name='sort_by'
                    class='text-sm border-gray-100 rounded'>
                    <option readonly>Sort</option>
                    <option value='title-asc'>Title, A-Z</option>
                    <option value='title-desc'>Title, Z-A</option>
                    <option value='price-asc'>Price, Low-High</option>
                    <option value='price-desc'>Price, High-Low</option>
                  </select>
                </div>
              </div>

              <div class='grid grid-cols-1 gap-px mt-4 bg-gray-200 border border-gray-200 sm:grid-cols-2 lg:grid-cols-3'></div>
              <Grid.Container gap={2} justify='flex-start'>
                {list.map((item, index) => (
                  <Grid xs={6} sm={3} key={index}>
                    <Card
                      hoverable
                      clickable
                      onClick={() => {
                        navigate_to_product_detail(item);
                      }}>
                      <Card.Body css={{ p: 0 }}>
                        <Card.Image
                          objectFit='cover'
                          src={'https://nextui.org' + item.img}
                          width='100%'
                          height={140}
                          alt={item.title}
                        />
                      </Card.Body>
                      <Card.Footer justify='flex-start'>
                        <Row wrap='wrap' justify='space-between'>
                          <Text b>{item.title}</Text>
                          <Text
                            css={{
                              color: '$accents4',
                              fontWeight: '$semibold',
                            }}>
                            {item.price}
                          </Text>
                        </Row>
                      </Card.Footer>
                    </Card>
                  </Grid>
                ))}
              </Grid.Container>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Product;
