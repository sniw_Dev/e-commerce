/** @format */

import React, { useState, useEffect } from 'react';
import { Button, Grid, Loading } from '@nextui-org/react';
import { useFormik } from 'formik';
import { UnLockIcon } from '../../assets/icons/UnLockIcon';
import { LockIcon } from '../../assets/icons/LockIcon';
import { registerUser } from '../../Redux/actions/Actionuser';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router-dom';

const Signup = (props) => {
  const [isloading, setLoading] = useState(false);
  const [showpassword, setShowpassword] = useState(false);
  const [showconfirmpassword, setShowconfirmpassword] = useState(false);
  const [error, setError] = useState('');
  const navigate = useNavigate();

  //Formik hook for handling form data

  //initial values for the form

  useEffect(() => {
    setLoading(false);

    if (props.success === 'REGISTER_SUCCESS') {
      navigate('/login');
    }

    if (props.errors) {
      console.log(props.errors);
      setError(props.errors);
    }
  }, [props]);

  const Initialvaules = {
    email: '',
    password: '',
    confirmpassword: '',
    fullname: '',
    phone: '',
  };

  //Submit handler for the form
  const onSubmit = (values) => {
    setLoading(true);
    values.phone = values.phone.toString();
    props.registerUser(values);
    // props.loginUser(values);
    console.log(values);
  };

  //Validation schema for the form

  const validate = (values) => {
    const errors = {};

    if (!values.email) {
      errors.email = 'Required';
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = 'Invalid email address';
    }

    if (!values.password) {
      errors.password = 'Required';
    }

    if (values.password.length < 6) {
      errors.password = 'Password must be at least 6 characters';
    }

    if (values.password !== values.confirmpassword) {
      errors.confirmpassword = 'Password does not match';
    }
    if (!values.fullname) {
      errors.fullname = 'Required';
    }
    if (!values.phone) {
      errors.phone = 'Required';
    }
    if (values.phone.toString().length !== 8) {
      console.log(values.phone.toString().length);
      errors.phone = 'Phone number must be 8 digits';
    }
    if (values.phone.length === 7) {
      if (!/^[925]\d{8}$/i.test(values.phone)) {
        errors.phone = 'unvalid phone number';
      }
    }

    return errors;
  };

  //Form istance

  const formik = useFormik({
    initialValues: Initialvaules,
    validate,
    onSubmit: onSubmit,
  });

  return (
    <div>
      <div class='max-w-screen-xl px-4 py-16 mx-auto sm:px-6 lg:px-8'>
        <div class='max-w-lg mx-auto'>
          <h1 class='text-2xl font-bold text-center text-indigo-600 sm:text-3xl'>
            Get started today
          </h1>

          <p class='max-w-md mx-auto mt-4 text-center text-gray-500'>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati
            sunt dolores deleniti inventore quaerat mollitia?
          </p>
          {props.errors &&
            Object.keys(props.errors).map((key, index) => {
              return (
                <div
                  class='p-4 text-red-700 border rounded border-red-900/10 bg-red-50'
                  role='alert'>
                  <p class='mt-1 text-xs'>{props.errors[key]}</p>
                </div>
              );
            })}

          <form
            onSubmit={formik.handleSubmit}
            class='p-8 mt-6 mb-0 space-y-4 rounded-lg shadow-2xl'>
            <p class='text-lg font-medium'>Sign up and join us</p>
            <div>
              <label
                for='fullname'
                class='text-sm font-medium  w-full block text-gray-700'
                style={{ textAlign: 'left' }}>
                Full Name
              </label>
              {formik.errors.fullname && formik.touched.fullname ? (
                <label
                  for='fullname'
                  class='text-sm font-medium  w-full block text-red-400'
                  style={{ textAlign: 'left' }}>
                  {formik.errors.fullname}
                </label>
              ) : null}
              <div class='relative mt-1'>
                <input
                  type='text'
                  id='fullname'
                  name='fullname'
                  class='w-full p-4 pr-12 text-sm border-gray-200 rounded-lg shadow-sm'
                  placeholder=' name'
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.fullname}
                />

                <span class='absolute inset-y-0 inline-flex items-center right-4'>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    class='w-5 h-5 text-gray-400'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'>
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                    />
                  </svg>
                </span>
              </div>
            </div>{' '}
            <div>
              <label
                for='email'
                class='text-sm font-medium  w-full block text-gray-700'
                style={{ textAlign: 'left' }}>
                Phone Number
              </label>
              {formik.errors.phone && formik.touched.phone ? (
                <label
                  for='password'
                  class='text-sm font-medium  w-full block text-red-400'
                  style={{ textAlign: 'left' }}>
                  {formik.errors.phone}
                </label>
              ) : null}
              <div class='relative mt-1'>
                <input
                  type='number'
                  id='phone'
                  name='phone'
                  class='w-full p-4 pr-12 text-sm border-gray-200 rounded-lg shadow-sm'
                  placeholder='Enter email'
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.phone}
                />

                <span class='absolute inset-y-0 inline-flex items-center right-4'>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    class='w-5 h-5 text-gray-400'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'>
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                    />
                  </svg>
                </span>
              </div>
            </div>{' '}
            <div>
              <label
                for='email'
                class='text-sm font-medium  w-full block text-gray-700'
                style={{ textAlign: 'left' }}>
                Email
              </label>
              {formik.errors.email && formik.touched.email ? (
                <label
                  for='password'
                  class='text-sm font-medium  w-full block text-red-400'
                  style={{ textAlign: 'left' }}>
                  {formik.errors.email}
                </label>
              ) : null}
              <div class='relative mt-1'>
                <input
                  type='email'
                  id='email'
                  name='email'
                  class='w-full p-4 pr-12 text-sm border-gray-200 rounded-lg shadow-sm'
                  placeholder='Enter email'
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                />

                <span class='absolute inset-y-0 inline-flex items-center right-4'>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    class='w-5 h-5 text-gray-400'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'>
                    <path
                      stroke-linecap='round'
                      stroke-linejoin='round'
                      stroke-width='2'
                      d='M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207'
                    />
                  </svg>
                </span>
              </div>
            </div>{' '}
            <div>
              <label
                for='password'
                class='text-sm font-medium  w-full block text-gray-700'
                style={{ textAlign: 'left' }}>
                Password
              </label>
              {formik.errors.password && formik.touched.password ? (
                <label
                  for='password'
                  class='text-sm font-medium  w-full block text-red-400'
                  style={{ textAlign: 'left' }}>
                  {formik.errors.password}
                </label>
              ) : null}
              <div class='relative mt-1'>
                <input
                  type={showpassword ? 'text' : 'password'}
                  id='password'
                  name='password'
                  class='w-full p-4 pr-12 text-sm border-gray-200 rounded-lg shadow-sm'
                  placeholder='Enter password'
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <span
                  class='absolute inset-y-0 inline-flex items-center right-4'
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    setShowpassword(!showpassword);
                  }}>
                  {showpassword ? (
                    <UnLockIcon fill='#9ea5b1' />
                  ) : (
                    <LockIcon fill={'#9ea5b1'} />
                  )}
                </span>
              </div>
            </div>
            <div>
              <label
                for='confirmpassword'
                class='text-sm font-medium  w-full block text-gray-700'
                style={{ textAlign: 'left' }}>
                Confirm Password
              </label>
              {formik.errors.confirmpassword &&
              formik.touched.confirmpassword ? (
                <label
                  for='confirmpassword'
                  class='text-sm font-medium  w-full block text-red-400'
                  style={{ textAlign: 'left' }}>
                  {formik.errors.confirmpassword}
                </label>
              ) : null}
              <div class='relative mt-1'>
                <input
                  type={showconfirmpassword ? 'text' : 'password'}
                  id='password'
                  name='confirmpassword'
                  class='w-full p-4 pr-12 text-sm border-gray-200 rounded-lg shadow-sm'
                  placeholder='Enter password'
                  value={formik.values.confirmpassword}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                <span
                  class='absolute inset-y-0 inline-flex items-center right-4'
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    setShowconfirmpassword(!showconfirmpassword);
                  }}>
                  {showconfirmpassword ? (
                    <UnLockIcon fill='#9ea5b1' />
                  ) : (
                    <LockIcon fill={'#9ea5b1'} />
                  )}
                </span>
              </div>
            </div>
            <button
              type='submit'
              class='block w-full px-5 py-3 text-sm font-medium text-white bg-indigo-600 rounded-lg'>
              {isloading ? (
                <Loading color='currentColor' size='sm' />
              ) : (
                <>Create account</>
              )}
            </button>
            <p class='text-sm text-center text-gray-500'>
              Have an account ?
              <a class='underline' href='login' style={{ marginLeft: '5px' }}>
                login
              </a>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};
Signup.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  success: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
  success: state.success,
});
export default connect(mapStateToProps, { registerUser })(Signup);
