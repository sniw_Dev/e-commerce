/** @format */

import React from 'react';
import MainCard from '../cards/MainCard/MainCard';
import { Autoplay, Navigation } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';
import { useSwiper } from 'swiper/react';
import { Card, Col, Row, Button, Text, Grid } from '@nextui-org/react';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
const HomeSlider = () => {
  const list = [
    {
      title: 'Orange',
      img: '/images/fruit-1.jpeg',
      price: '5.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Tangerine',
      img: '/images/fruit-2.jpeg',
      price: '3.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Raspberry',
      img: '/images/fruit-3.jpeg',
      price: '10.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon',
      img: '/images/fruit-4.jpeg',
      price: '5.30',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Advocato',
      img: '/images/fruit-5.jpeg',
      price: '15.70',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Lemon 2',
      img: '/images/fruit-6.jpeg',
      price: '8.00',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Banana',
      img: '/images/fruit-7.jpeg',
      price: '7.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Watermelon',
      img: '/images/fruit-8.jpeg',
      price: '12.20',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
    {
      title: 'Orange',
      img: '/images/fruit-1.jpeg',
      price: '5.50',
      description:
        'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
    },
  ];

  return (
    <Card
      isHoverable
      variant='bordered'
      css={{
        mw: '90%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '1rem',
      }}>
      <Card.Body>
        <div
          style={{
            position: 'relative',
            padding: '20px',
            marginBottom: '30px',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <div
            style={{
              position: 'relative',
              padding: '20px',
              width: '100%',
            }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
              }}>
              <Text css={{ color: '#333' }} size={20} transform='uppercase'>
                Categorie Name
              </Text>
              <Text css={{ color: '#333' }} size={20} transform='uppercase'>
                See All
              </Text>
            </div>
            {/* <Swiper
              // install Swiper modules
              modules={[Autoplay, Navigation]}
              spaceBetween={50}
              slidesPerView={3}
              onSwiper={(swiper) => {
                // Delay execution for the refs to be defined
              }}
              onSlideChange={() => console.log('slide change')}>
              {list.map((element, index) => (
                <SwiperSlide>
                  <MainCard element={element} />
                </SwiperSlide>
              ))}
            </Swiper> */}
            <Grid.Container gap={2} justify='center'>
              {list.map((element, index) => (
                <Grid sm={4} xs={12}>
                  <MainCard element={element} />
                </Grid>
              ))}
            </Grid.Container>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
};

export default HomeSlider;
