/** @format */

import React from 'react';
import { Card, Col, Row, Button, Text } from '@nextui-org/react';

const MainCard = ({ element }) => {
  return (
    <div>
      {' '}
      <div class='py-6'>
        <div class='flex max-w-sm min-w-full bg-white shadow-lg rounded-lg overflow-hidden'>
          <div
            class='w-1/3 bg-cover bg-center'
            style={{
              'background-image': `url(https://nextui.org/${element.img})`,
            }}></div>
          <div class='w-2/3 p-4'>
            <h1 class='text-gray-900 font-bold text-2xl'>{element.title}</h1>
            <p class='mt-2 text-gray-600 text-sm'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit In odit
              exercitationem fuga id nam quia
            </p>
            <div class='flex item-center mt-2'>
              <svg
                class='w-5 h-5 fill-current text-yellow-500'
                viewBox='0 0 24 24'>
                <path d='M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z' />
              </svg>
              <svg
                class='w-5 h-5 fill-current  text-yellow-500'
                viewBox='0 0 24 24'>
                <path d='M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z' />
              </svg>
              <svg
                class='w-5 h-5 fill-current  text-yellow-500'
                viewBox='0 0 24 24'>
                <path d='M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z' />
              </svg>
              <svg
                class='w-5 h-5 fill-current text-yellow-500'
                viewBox='0 0 24 24'>
                <path d='M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z' />
              </svg>
              <svg
                class='w-5 h-5 fill-current text-yellow-500'
                viewBox='0 0 24 24'>
                <path d='M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z' />
              </svg>
            </div>
            <div class='flex item-center justify-between mt-3'>
              <h1 class='text-gray-700 font-bold text-xl'>
                {element.price} TND
              </h1>
              <button class='px-3 py-2 bg-gray-800 text-white text-xs font-bold uppercase rounded'>
                Add to Card
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>

    // <Card css={{ w: '100%', h: '280px' }}>
    //   <Card.Header css={{ position: 'absolute', zIndex: 1, top: 5 }}>
    //     <Col>
    //       <Text
    //         size={14}
    //         weight='bold'
    //         transform='uppercase'
    //         color='#fff'></Text>
    //     </Col>
    //   </Card.Header>
    //   <Card.Body css={{ p: 0 }}>
    //     <Card.Image
    //       src={`https://nextui.org/${element.img}`}
    //       width='100%'
    //       height='100%'
    //       objectFit='cover'
    //       alt='Card example background'
    //     />
    //   </Card.Body>
    //   <Card.Footer
    //     isBlurred
    //     css={{
    //       position: 'absolute',
    //       bgBlur: '#ffffff66',
    //       borderTop: '$borderWeights$light solid rgba(255, 255, 255, 0.2)',
    //       bottom: 0,
    //       zIndex: 1,
    //     }}>
    //     <Row>
    //       <Col>
    //         <Text
    //           css={{ color: '#fff' }}
    //           size={20}
    //           weight='bold'
    //           transform='uppercase'>
    //           {element.title}
    //         </Text>
    //       </Col>
    //       <Col>
    //         <Row justify='flex-end'>
    //           <Button flat auto rounded color='secondary'>
    //             <Text
    //               css={{ color: '#fff' }}
    //               size={20}
    //               weight='bold'
    //               transform='uppercase'>
    //               {element.price}{' '}
    //             </Text>
    //           </Button>
    //         </Row>
    //       </Col>
    //     </Row>
    //   </Card.Footer>
    // </Card>
  );
};

export default MainCard;
