/** @format */

import React, { useEffect } from 'react';
import { Card, Col, Row, Button, Text, Grid } from '@nextui-org/react';
import { useSelector, useDispatch } from 'react-redux';

export const Card4 = ({ isactive, element }) => (
  <Card
    css={{
      w: '100%',
      h: '200px',
      border: isactive ? 'solid' : 'none',
      borderColor: '#e11d48',
      borderWidth: '5px',
    }}>
    <Card.Header
      isBlurred
      css={{
        position: 'absolute',
        zIndex: 1,
        bottom: 5,
        textAlign: 'left',
        bgBlur: '#ffffff66',
        borderTop: '$borderWeights$light solid rgba(255, 255, 255, 0.2)',
      }}>
      <Col>
        <Text size={12} weight='bold' transform='uppercase' color='#fff'>
          New
        </Text>
        <Text h3 color='#fff' weight='bold' transform='uppercase'>
          Acme camera
        </Text>
      </Col>
    </Card.Header>
    <Card.Body css={{ p: 0 }}>
      <Card.Image
        src={element.image}
        width='100%'
        height='100%'
        objectFit='cover'
        alt='Card example background'
      />
    </Card.Body>
  </Card>
);

const HeroCards = ({ isactive, updateIndex, element }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    isactive && element && dispatch(setHero(element.image));

    setTimeout(function () {
      // Do Something Here
      // Then recall the parent function to
      // create a recursive loop.
      updateIndex();
    }, 3000);
  }, [isactive]);

  const setHero = (img) => {
    return {
      type: 'UPDATE_HERO',
      payload: img,
    };
  };
  return (
    // <Card css={{ mw: '300px', mh: '150px' }}>
    //   <Card.Header
    //     css={{ position: 'absolute', zIndex: 1, left: 5 }}
    //     style={{ textAlign: 'left' }}>
    //     <Col>
    //       <Text size={12} weight='bold' transform='uppercase' color='#ffffffAA'>
    //         Supercharged
    //       </Text>
    //       <Text h4 color='white'>
    //         Creates beauty like a beast
    //       </Text>
    //     </Col>
    //   </Card.Header>
    //   <Card.Image
    //     src='https://canvas-cdn-prod.azureedge.net/assets/3a/63/3a634fa4-d15f-4128-aa90-077088f8fd1b.png?n=New_MS365_4-26-22.png'
    //     width='100%'
    //     height='100%'
    //     objectFit='cover'
    //     alt='Card image background'
    //   />
    // </Card>
    <>{element && <Card4 isactive={isactive} element={element} />}</>
  );
};

export default HeroCards;
