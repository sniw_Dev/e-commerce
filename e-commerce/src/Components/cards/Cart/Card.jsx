/** @format */

import React from 'react';
import { Card, Grid, Text, Link, Button } from '@nextui-org/react';
const AddIcon = ({ fill = 'currentColor', filled }) => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      className='h-8 w-8'
      fill='red'
      viewBox='0 0 24 24'
      stroke='currentColor'
      strokeWidth={2}>
      <path
        strokeLinecap='round'
        strokeLinejoin='round'
        d='M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z'
      />
    </svg>
  );
};

const MinusIcon = () => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      className='h-8 w-8'
      fill='red'
      viewBox='0 0 24 24'
      stroke='currentColor'
      strokeWidth={2}>
      <path
        strokeLinecap='round'
        strokeLinejoin='round'
        d='M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z'
      />
    </svg>
  );
};

const TrashIcon = () => {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      className='h-8 w-8'
      fill='red'
      viewBox='0 0 24 24'
      stroke='currentColor'
      strokeWidth={2}>
      <path
        strokeLinecap='round'
        strokeLinejoin='round'
        d='M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z'
      />
    </svg>
  );
};
const Cardcart = () => {
  return (
    <Card
      css={{ p: '$6', mw: '100%' }}
      style={{
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        flexWrap: 'wrap',
      }}>
      <Card.Header
        style={{
          display: 'flex',
          justifyContent: 'space-around',
          alignItems: 'center',
          flexWrap: 'wrap',
        }}>
        <Grid.Container css={{ pl: '$6' }}>
          <Grid
            xs={12}
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}>
            <img
              alt='nextui logo'
              src='https://avatars.githubusercontent.com/u/86160567?s=200&v=4'
              width='60px'
              height='60px'
            />
            <Text h2 size={20} css={{ lineHeight: '$sm' }}>
              Next UI
            </Text>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <Button auto color='error' icon={<AddIcon />} />
              <Text h2 size={20} css={{ lineHeight: '$sm' }}>
                0
              </Text>
              <Button auto color='error' icon={<MinusIcon />} />
            </div>
            <Button auto color='error' icon={<TrashIcon />} />
          </Grid>
        </Grid.Container>
      </Card.Header>
    </Card>
  );
};

export default Cardcart;
