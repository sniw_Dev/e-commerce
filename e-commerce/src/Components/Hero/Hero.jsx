/** @format */

import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

const Hero = () => {
  const img = useSelector((state) => state.hero.img);
  useEffect(() => {
    console.log(img);
  }, [img]);
  return (
    <div>
      <section class='relative bg-white'>
        <img
          class='absolute  inset-0 object-[75%] sm:object-[25%] object-cover w-full h-full opacity-25 sm:opacity-100'
          src={img}
          alt='Couple on a bed with a dog'
        />

        <div class='hidden sm:block sm:inset-0  bg-gray-900 bg-opacity-50 sm:absolute sm:bg-gradient-to-r sm:from-gray-900 sm:to-transparent'></div>

        <div class='relative max-w-screen-xl px-4 py-32 mx-auto lg:h-screen lg:items-center lg:flex'>
          <div class='max-w-xl text-center sm:text-left'>
            <h1 class='text-3xl  text-white font-extrabold sm:text-5xl'>
              Let us find your
              <strong class='font-extrabold text-rose-700 sm:block'>
                Forever Home.
              </strong>
            </h1>

            <p class='max-w-lg text-white mt-4 sm:leading-relaxed sm:text-xl'>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nesciunt
              illo tenetur fuga ducimus numquam ea!
            </p>

            <div class='flex flex-wrap gap-4 mt-8 text-center'>
              <a
                class='block w-full px-12 py-3 text-sm font-medium text-white rounded shadow bg-rose-600 sm:w-auto active:bg-rose-500 hover:bg-rose-700 focus:outline-none focus:ring'
                href='/get-started'>
                Get Started
              </a>

              <a
                class='block w-full px-12 py-3 text-sm font-medium bg-white rounded shadow text-rose-600 sm:w-auto hover:text-rose-700 active:text-rose-500 focus:outline-none focus:ring'
                href='/about'>
                Learn More
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Hero;
