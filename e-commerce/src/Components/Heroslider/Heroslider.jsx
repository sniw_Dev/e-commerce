/** @format */

import React, { useState, useEffect } from 'react';
import HeroCards from '../cards/HeroCards/HeroCards';
import { Autoplay, Navigation } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';
import { useSwiper } from 'swiper/react';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';

const Heroslider = () => {
  const [ActiveIndex, setActiveIndex] = useState(0);

  const elements = [
    {
      name: 'Categorie1',
      image:
        'https://asiacorpo.com/wp-content/uploads/2019/10/Products-Food-Products.png',
      description: '',
    },
    {
      name: 'Categorie2',
      image:
        'https://i.insider.com/58e3858577bb7028008b603a?width=1200&format=jpeg',
      description: '',
    },
    {
      name: 'Categorie3',
      image: 'https://www.akfood.in/image/home-products/8.png',
      description: '',
    },
    {
      name: 'Categorie4',
      image:
        'https://static.timesofisrael.com/www/uploads/2022/04/F220425AS05.jpg',
      description: '',
    },
    {
      name: 'Categorie5',
      image:
        'https://dmrqkbkq8el9i.cloudfront.net/Pictures/1024x536/5/4/8/245548_crispsnutssnacks_423369_crop.jpg',
      description: '',
    },
    {
      name: 'Categorie6',
      image:
        'https://www.snackandbakery.com/ext/resources/images/snackproducts.jpg?1515098334',
      description: '',
    },
  ];

  const ChangeIndex = () => {
    if (ActiveIndex === elements.length - 1) {
      setActiveIndex(0);
    } else {
      setActiveIndex(ActiveIndex + 1);
    }
  };
  return (
    <>
      <Swiper
        style={{ position: 'relative', bottom: '200px' }}
        navigation={{
          prevEl: '.arrows_container_prev',
          nextEl: '.arrows_container_next',
        }}
        // install Swiper modules
        modules={[Autoplay, Navigation]}
        spaceBetween={50}
        autoplay={{
          delay: '6000',
          disableOnInteraction: false,
        }}
        slidesPerView={4}
        onSwiper={(swiper) => {
          // Delay execution for the refs to be defined
        }}
        onSlideChange={() => console.log('slide change')}>
        {elements.map((element, index) => (
          <SwiperSlide>
            <HeroCards
              isactive={ActiveIndex === index}
              element={element}
              updateIndex={ChangeIndex}
            />
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default Heroslider;
