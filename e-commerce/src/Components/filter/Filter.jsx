/** @format */

import React from 'react';
import { Card } from '@nextui-org/react';
import { Grid, Row, Text } from '@nextui-org/react';
import CategorieCard from '../cards/categorie/CategorieCard';
import Search from '../inputs/Search/Search';
const Filter = () => {
  return (
    <div>
      <Grid.Container gap={2} justify='center'>
        <Grid xs={12} sm={8}>
          <Search />
        </Grid>
      </Grid.Container>
    </div>
  );
};

export default Filter;
